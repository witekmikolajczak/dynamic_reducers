import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';

import { getFilterCollection } from './api/api';
import { wrappedSlice } from './redux/reducer/filterReducer';
import { useAppDispatch, useAppSelector } from './redux/reduxHook';
import { changeRemote } from './redux/reducer/filterReducer';
import store from './redux/store';
import { useDispatch } from 'react-redux';

function App() {
  const dispatch = useDispatch();
  const selector = useAppSelector((state) => state);
  console.log(selector);
  useEffect(() => {
    console.log(wrappedSlice({}, {}).caseReducers);

    dispatch({ type: 'filter/changeRemote', payload: 'yes' });
    store.dispatch({ type: 'filter/category', payload: 'Active' });
  }, [dispatch]);

  // const [filterCollection, setFilterCollection] = useState();

  // useEffect(() => {
  //   getFilterCollection().then((res) => setFilterCollection(res));
  // }, []);

  // const initialState = {
  //   category: 'initialState',
  // };

  // const reducerObjects = {
  //   category: function (state: any, action: any) {
  //     return { ...state, category: action.payload };
  //   },
  // };
  // const makeReducers = wrappedSlice(reducerObjects, initialState);
  // console.log(makeReducers);
  // const objectToDispatch = 'not initialState';
  // // useEffect(() => {
  // //   const test = makeReducers.actions.loadFilterOptions;
  // //   console.log(test);
  // // }, [makeReducers]);

  // useEffect(() => {
  //   console.log('DISPATCH');

  //   dispatch(changeRemote(objectToDispatch));
  // }, []);
  // useEffect(() => {
  //   console.log('SELECTOR');
  // }, [selector, makeReducers, dispatch]);
  // console.log(selector);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
