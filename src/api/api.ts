export const getFilterCollection = async () => {
  const response = await fetch('http://147.182.252.74:4003/parse/functions/offerGetFilter', {
    method: 'POST',
    headers: {
      // REACT_APP_GRAPHQL_URL=http://147.182.252.74:4003/graphql
      // REACT_APP_PARSE_SERVER_URL=http://147.182.252.74:4003/parse
      'X-Parse-Application-Id': 'j_ux_pro',
    },
  }).then((res) => res.json());

  return response;
};
