import { useEffect } from 'react';
//redux
import { injectAsyncReducer } from '../redux/store';
// import { loadFilterOptions } from '../../redux/reducer/filterOptionReducer';
// import { addReducers } from '../../store';
import { createGenericSlice, GenericState, wrappedSlice } from '../redux/reducer/filterReducer';
import { Slice } from '@reduxjs/toolkit';

const apiObject: any = {
  location: { key: 'location' },
  remote: { key: 'remote' },
  test: { key: 'test' },
};

export const useFilter = () => {
  interface DynamicReducerInterface {
    makeFunction: () => void;
    createReducer: () => void;
  }
  class CreateDynamicReducer implements DynamicReducerInterface {
    public functionCollection: any;
    private object: any[];
    //@ts-ignore
    public reducerCollection: Slice<GenericState<[]>, any, string>;
    constructor(object: any[]) {
      this.object = object;
      this.functionCollection = {};
    }

    makeFunction(): void {
      for (let i = 0; i < this.object.length; i++) {
        //make function by given keys
        this.functionCollection[this.object[i].key] = new Function('state', 'action', `return {...state, ${this.object[i].key}: action.payload}`);
      }
    }

    createReducer(): void {
      //collection of functions
      this.reducerCollection = wrappedSlice(this.functionCollection, this.object);

      //create new reducer but only with location key
      injectAsyncReducer(this.reducerCollection.caseReducers.location);
    }
  }

  const createReducer = new CreateDynamicReducer(apiObject);
  var keys: any = [];
  Object.entries(apiObject).forEach(([key, value]: any, index) => {
    keys.push(value.key);
  });
  createReducer.makeFunction();

  createReducer.createReducer();
  const test = createReducer.reducerCollection.caseReducers.loadFilterOptions({ id: '1', name: 'test' }, { payload: { id: '2', name: 'test2' } });
};
