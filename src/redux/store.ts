import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import createReducer from './rootReducer';
import { fitReducer } from './middleware';

export function initStore() {
  return configureStore({
    reducer: createReducer(),
    middleware: (getDefaultMiddleware) => [...getDefaultMiddleware(), fitReducer],
  });
}
const store = initStore();

export function injectAsyncReducer(asyncReducer = {}) {
  return store.replaceReducer(createReducer(asyncReducer));
}

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;

export default store;
