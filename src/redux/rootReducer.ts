import { combineReducers } from 'redux';
import { createGenericSlice } from './reducer/filterReducer';
import filterReducer from './reducer/filterReducer';
import { wrappedSlice } from './reducer/filterReducer';
// api

const initialReducers = {
  remote: filterReducer,
};
export const createReducer = (asyncReducers = {}) => {
  const initialState = {
    category: 'initialState',
  };

  const reducerObjects = {
    category: function (state: any, action: any) {
      return { ...state, category: action.payload };
    },
    location: function (state: any, action: any) {
      return { ...state, category: action.payload };
    },
  };
  const makeReducers = wrappedSlice(reducerObjects, initialState);
  asyncReducers = makeReducers.caseReducers;
  console.log(asyncReducers);
  console.log(initialReducers);

  return combineReducers({
    ...initialReducers,
    ...asyncReducers,
  });
};

export default createReducer;
