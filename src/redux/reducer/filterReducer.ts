import { createSlice, SliceCaseReducers, ValidateSliceCaseReducers } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

export interface GenericState<T> {}
export const createGenericSlice = <T, Reducers extends SliceCaseReducers<GenericState<T>>>({
  name = 'filter',
  initialState = 'initialState',
  reducers,
}: {
  name: string;
  initialState: GenericState<T>;
  reducers: ValidateSliceCaseReducers<GenericState<T>, Reducers>;
}) => {
  console.log(name, reducers);

  return createSlice({
    name,
    initialState,
    reducers: {
      // loadFilterOptions: (state, action: PayloadAction<any>) => {
      //   state = action.payload;
      //   return state;
      // },
      ...reducers,
    },
  });
};

export const wrappedSlice = (reducers: any, initialState: any) =>
  createGenericSlice({
    name: 'filter',
    initialState: initialState as GenericState<[]>,
    reducers: {
      ...reducers,
    },
  });

const filterReducer = createSlice({
  name: 'filter',
  initialState: 'initialState',
  reducers: {
    changeRemote: (state, action: PayloadAction<any>) => {
      state = action.payload;
      return state;
    },
  },
});

export const { changeRemote } = filterReducer.actions;

export default filterReducer.reducer;
